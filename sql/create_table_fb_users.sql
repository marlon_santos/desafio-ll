CREATE TABLE `fb_users` (
  `facebookId` varchar(50) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`facebookId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;