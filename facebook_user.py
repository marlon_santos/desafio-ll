#coding:utf-8
#!/usr/bin/env python
import web
import MySQLdb
import json
import urllib2
import datetime

#acessando a url http://localhost:8080/users chama a classe list_users
#acessando a url http://localhost:8080/users/{id} chama a classe get_user passando {id} como parâmetro via GET
urls = (
    '/person', 'list_users',
    '/person/list/(.*)', 'list_users_limit',
    '/person/add/(.*)', 'add_user',
    '/person/fbget/(.*)', 'get_from_fb',
    '/person/del/(.*)', 'del_user'
)

app = web.application(urls, globals())

conn = MySQLdb.connect('localhost','root', 'mysql', 'test')
cursor = conn.cursor()

class list_users:
    def GET(self):
        try:
            output = ("HTTP 200\n")
            SQLlist = 'SELECT * FROM fb_users'
            cursor.execute(SQLlist)
            data = cursor.fetchall()
            person = []
            for row in data:
                person_data = {}
                person_data = {'facebookId':row[0], 'username':row[1], 'name':row[2].decode('iso-8859-1').encode('utf-8'), 'gender':row[3]}
                person.append(person_data)
            output += json.dumps(person,indent=4, separators=(',',': '))
            write_log("[OK] Listed all users")
        except:
            output = "HTTP 200 [{'err':'limit parameter is invalid!'}]"
            write_log("[ERROR] Users not listed")
        return output

class get_from_fb:
    def GET(self, facebookId):
        output = '';
        if facebookId == '':
            output = 'HTTP 404'
        else:
            facebookUrl = 'https://graph.facebook.com/'+facebookId
            req = urllib2.Request(facebookUrl)
            opener = urllib2.build_opener()
            f = opener.open(req)
            output = f.read()

        return output

class list_users_limit:
    def GET(self, limit):
        try:
            output = ("HTTP 200\n")
            if limit == '':
                SQLlist = 'SELECT * FROM fb_users'
            else:
                SQLlist = 'SELECT * FROM fb_users LIMIT 0,%s' % limit

            cursor.execute(SQLlist)
            data = cursor.fetchall()
            person = []
            for row in data:
                person_data = {}
                person_data = {'facebookId':row[0], 'username':row[1], 'name':row[2].decode('iso-8859-1').encode('utf-8'), 'gender':row[3]}
                person.append(person_data)
            output += json.dumps(person,indent=4, separators=(',',': '))
            write_log("[OK] Listed %s users" % limit)
        except:
            output = "HTTP 200 [{'err':'limit parameter is invalid!'}]"
            write_log("[ERROR] Users not listed")
        return output

class add_user:
    def GET(self, facebookId):
        output = ''
        if facebookId == '':
            output = 'HTTP 404'
        else:
            try:
                facebookUrl = 'https://graph.facebook.com/'+facebookId
                req = urllib2.Request(facebookUrl)
                opener = urllib2.build_opener()
                f = opener.open(req)
                opener_data = f.read()
                json_data = json.loads(opener_data)
                dictInsert = {}
                for data in json_data:
                    dictInsert[data] = json_data[data]
                    
                sqlInsert = "INSERT INTO fb_users(facebookId,username,name,gender) VALUES('%s','%s','%s','%s')" % (dictInsert['id'], dictInsert['username'], dictInsert['name'], dictInsert['gender'])
                cursor.execute(sqlInsert)
                conn.commit()
                output = 'HTTP 201'
                write_log("[OK] User with ID '%s' added" % facebookId)
            except:
                conn.rollback()
                output = "HTTP 201 [{'err':'registry not inserted'}]"
                write_log("[ERROR] User with ID '%s' not added" % facebookId)

        return output

class del_user:
    def GET(self, facebookId):
        if facebookId == '':
            output = 'HTTP 404'
        else:
            sqlDelete = "DELETE FROM fb_users WHERE facebookId = '%s'" % facebookId
            try:
                cursor.execute(sqlDelete)
                conn.commit()
                output = 'HTTP 204'
                write_log("[OK] User with ID '%s' deleted" % facebookId)
            except:
                conn.rollback()
                output = "HTTP 204 [{'err':'registry not deleted'}]"
                write_log("[ERROR] User with ID '%s' not deleted" % facebookId)

        return output

def write_log(log):
    f = open("log.txt","a")
    log = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+" - " + log +"\n"
    f.write(log)
    f.close()

if __name__ == "__main__":
    app.run()